use std::io;
use std::ops;
use std::convert::From;

#[derive(Copy, Clone)]
struct Coors(i32, i32);

impl Coors {
    fn for_neighbors<F>(&self, mut action: F) where F: FnMut(Coors) {
        action(Coors(self.0, self.1 - 1));
        action(Coors(self.0, self.1 + 1));
        action(Coors(self.0 - 1, self.1));
        action(Coors(self.0 + 1, self.1));
    }
}

enum Direction {
    Up,
    Right,
    Down,
    Left,
}

impl From<Direction> for Coors {
    fn from(dir: Direction) -> Self {
        match dir {
            Direction::Up    => Coors(-1,  0),
            Direction::Right => Coors( 0,  1),
            Direction::Down  => Coors( 1,  0),
            Direction::Left  => Coors( 0, -1),
        }
    }
}

impl ops::Add<Coors> for Coors {
    type Output = Coors;
    fn add(self, other: Coors) -> Self::Output {
        Coors(self.0 + other.0, self.1 + other.1)
    }
}

impl ops::Sub<Coors> for Coors {
    type Output = Coors;
    fn sub(self, other: Coors) -> Self::Output {
        Coors(self.0 - other.0, self.1 - other.1)
    }
}

impl ops::Mul<i32> for Coors {
    type Output = Coors;
    fn mul(self, other: i32) -> Self::Output {
        Coors(self.0 * other, self.1 * other)
    }
}

fn single_step(d: Coors) -> Option<(Coors, i32)> {
    match d {
        Coors(0, 0) => None,
        Coors(x, 0) => Some((Coors(x.signum(), 0), x.abs())),
        Coors(0, y) => Some((Coors(0, y.signum()), y.abs())),
        _ => None,
    }
}

enum PathContent {
    Box,
    Empty { accessible: bool },
}

enum Field {
    Wall,
    Path { target: bool, content: PathContent },
}

struct Board {
    fields: Vec<Vec<Field>>,
    player: Coors,
}

#[derive(Debug)]
enum MoveError {
    NotStraight,
    NotBox,
    NotAccessible,
    WayBlocked,
}

impl Board {
    fn size(&self) -> Coors {
        let x = self.fields.len();
        assert!(x > 0, "Getting size of empty map");
        let y = self.fields[0].len();
        assert!(y > 0, "Getting size of empty map");
        Coors(x as i32, y as i32)
    }
    fn wrap_coors(&self, coors: Coors) -> Coors {
        let size = self.size();
        Coors(
            (coors.0 % size.0 + size.0) % size.0,
            (coors.1 % size.1 + size.1) % size.1,
        )
    }
    fn foreach_mut<F>(&mut self, mut action: F) where F: FnMut(&mut Field) {
        for row in self.fields.iter_mut() {
            for f in row.iter_mut() {
                action(f);
            }
        }
    }
    fn foreach<F>(&self, mut action: F) where F: FnMut(&Field) {
        for row in self.fields.iter() {
            for f in row.iter() {
                action(f);
            }
        }
    }
    fn recalculate_accessibility(&mut self) {
        // clear previous accessibility info
        self.foreach_mut(|field| {
            if let Field::Path{ target: _, content: PathContent::Empty{ accessible: x } } = field {
                *x = false;
            };
        });
        // calculate new accessibility
        fn access(b: &mut Board, c: Coors) {
            let field = &mut b[c];
            if let Field::Path{ target: _, content: PathContent::Empty{ accessible: x @ false } } = field {
                *x = true;
                c.for_neighbors(|n| access(b, n));
            };
        }
        access(self, self.player);
    }
    fn move_box(&mut self, a: Coors, b: Coors) -> Result<(), MoveError> {
        match single_step(b - a) {
            Some((step, count)) => {
                // check if it's a box and if it's accessible
                match &self[a] {
                     Field::Path{ target: _, content: PathContent::Box } => (),
                     _ => return Err(MoveError::NotBox),
                }
                match &self[a - step] {
                    Field::Path{ target: _, content: PathContent::Empty{ accessible: true } } => (),
                     _ => return Err(MoveError::NotAccessible),
                }
                let mut t_coors = None;
                for i in 1 ..= count {
                    let c_coors = a + step * i;
                    if let Field::Path{ target: _, content: PathContent::Empty{ accessible: _ } } = &mut self[c_coors] {
                        t_coors = Some(c_coors);
                    } else {
                        break;
                    }
                }
                match t_coors {
                    Some(t_coors) => {
                        // shouldn't happen
                        fn fail() -> ! {
                            panic!("board somehow changed during move calculation");
                        }

                        // remove the box from its original position
                        match &mut self[a] {
                            Field::Path{ target: _, content: f_cont } => *f_cont = PathContent::Empty{ accessible: true },
                            _ => fail(),
                        }

                        // place the box to its new position
                        match &mut self[t_coors] {
                            Field::Path{ target: _, content: t_cont } => *t_cont = PathContent::Box,
                            _ => fail(),
                        }

                        // reposition the player and recalculate accessibility
                        self.player = t_coors - step;
                        self.recalculate_accessibility();

                        Ok(())
                    },
                    None => Err(MoveError::WayBlocked),
                }
            },
            None => Err(MoveError::NotStraight),
        }
    }
    fn win(&self) -> bool {
        let mut win = true;
        self.foreach(|f| if let Field::Path { target: true, content: PathContent::Empty { accessible: _ }} = f {
            win = false;
        });
        return win;
    }
}

impl ops::Index<Coors> for Board {
    type Output = Field;
    fn index(&self, coors: Coors) -> &Self::Output {
        let coors = self.wrap_coors(coors);
        &self.fields[coors.0 as usize][coors.1 as usize]
    }
}

impl ops::IndexMut<Coors> for Board {
    fn index_mut(&mut self, coors: Coors) -> &mut Self::Output {
        let coors = self.wrap_coors(coors);
        &mut self.fields[coors.0 as usize][coors.1 as usize]
    }
}

fn gen_test_board() -> Board {
    Board {
        fields: vec![
            vec![
                Field::Wall,
                Field::Wall,
                Field::Wall,
                Field::Wall,
                Field::Wall,
                Field::Wall,
            ],
            vec! [
                Field::Wall,
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Wall,
            ],
            vec! [
                Field::Wall,
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Box },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Wall,
            ],
            vec! [
                Field::Wall,
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target:  true, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Wall,
            ],
            vec! [
                Field::Wall,
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Path { target: false, content: PathContent::Empty { accessible: true } },
                Field::Wall,
            ],
            vec! [
                Field::Wall,
                Field::Wall,
                Field::Wall,
                Field::Wall,
                Field::Wall,
                Field::Wall,
            ],
        ],
        player: Coors(1, 1),
    }
}

impl Board {
    fn print(&self) {
        for row in self.fields.iter() {
            for f in row.iter() {
                print!(" {}", match f {
                    Field::Wall => "#",
                    Field::Path{ target: false, content: PathContent::Box } => "x",
                    Field::Path{ target:  true, content: PathContent::Box } => "X",
                    Field::Path{ target: false, content: PathContent::Empty{ accessible: _ } } => ".",
                    Field::Path{ target:  true, content: PathContent::Empty{ accessible: _ } } => "_",
                });
            }
            println!();
        }
    }
}

fn main() {
    let mut b = gen_test_board();
    loop {
        b.print();
        if b.win() {
            println!("win");
            break;
        }
        let mut line = String::new();
        io::stdin()
            .read_line(&mut line)
            .expect("Failed to read line");
        if line.trim() == "new" {
            b = gen_test_board();
        } else {
            let ww: Vec<i32> = line.trim().split(" ").map(|x| x.parse::<i32>().expect("not i32 in input")).collect();
            assert!(ww.len() == 4, "wrong input");
            match b.move_box(
                Coors(ww[0], ww[1]),
                Coors(ww[2], ww[3]),
            ) {
                Ok(_) => (),
                Err(err) => println!("{:?}", err),
            }
        }
    }
}
